/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import prompt from '@system.prompt';

@Component
export struct SwipeLayoutPullOut {
  @Link parentIsTouch: boolean
  private mDuration= 200
//  @BuilderParam surfaceView: () => {};
//  @BuilderParam bottomViewRight: () => {};
//  @BuilderParam bottomViewLeft: () => {};
//  @BuilderParam bottomViewTop: () => {};
//  @BuilderParam bottomViewBottom: () => {};
  private sX: number ;
  private sY: number ;
  private mWidth = '100%'
  private mHeight = 90
  private bottomViewRightW: number = 180
  private bottomViewLeftW: number = 120
  @State private offsetX: number= 0
  @State private offsetY: number= 0
  private rightSlideEnable= true;
  private leftSlideEnable= true;
  private previousX = 0;
  private enableSlide: boolean[]= new Array(false, false, true, false)

  build() {
    Column() {
      Stack() {
        Row() {
          Blank()
          Column() {
            Row() {
              Image($r('app.media.magnifier'))
                .backgroundColor('#e8eca5')
                .width('33.33%')
                .height('100%')
                .padding(8)
                .objectFit(ImageFit.Contain)
                .onClick(() => {
                  prompt.showToast({
                    message: "Magnifier",
                    duration: 1500,
                  });
                })
              Image($r('app.media.star'))
                .backgroundColor('#0dd398')
                .height('100%')
                .width('33.33%')
                .padding(8)
                .objectFit(ImageFit.Contain)
                .onClick(() => {
                  prompt.showToast({
                    message: "Star",
                    duration: 1500,
                  });
                })
              Image($r('app.media.trash'))
                .backgroundColor('#db0f4f')
                .width('33.33%')
                .height('100%')
                .padding(8)
                .objectFit(ImageFit.Contain)
                .onClick(() => {
                  prompt.showToast({
                    message: "Trash Bin",
                    duration: 1500,
                  });
                })
            }
            .height(this.mHeight)
            .width(this.bottomViewRightW)
          }
        }.width('100%')


        Column() {
          Text('理解（りかい）されるということは、一種（いっしゅ）の贅沢（ぜいたく）である。')
            .fontSize('20')
            .width('100%')
            .height(this.mHeight)
            .fontColor('#000000')
            .padding(5)
            .maxLines(1)
            .onClick(() => {
              prompt.showToast({
                message: "Click on surface",
                duration: 1500,
              });
            })
            .backgroundColor('#ffffff')
        }.onTouch((event: TouchEvent) => {
          console.info('move----pull out----')
          if (event.type === TouchType.Down) {
            this.parentIsTouch = false
            this.sX = event.touches[0].screenX
            this.sY = event.touches[0].screenY
          }

          if (event.type === TouchType.Move) {
            let moveX = event.touches[0].screenX - this.sX;
            let moveY = event.touches[0].screenY - this.sY;
            if (Math.abs(moveX) > Math.abs(moveY)) { // 横向滑动
              if (this.offsetY == 0) {
                if (moveX < 0 && moveY < 30 && moveY > -30) { //左滑

                  //左侧菜单处于打开状态，开始关闭
                  if (this.offsetX > 0) {
                    this.rightSlideEnable = false
                    if (-moveX < this.bottomViewLeftW) {
                      this.offsetX = moveX + this.bottomViewLeftW
                    } else {
                      this.offsetX = 0
                    }
                    return
                  }

                  //当前移动距离小于右侧View的宽度，并且没有处于刚好拉出的状态
                  if (!this.enableSlide[2]) {
                    return
                  }
                  if (this.rightSlideEnable) {
                    if (this.offsetX <= 0 && this.offsetX > -this.bottomViewRightW && -moveX <= this.bottomViewRightW) {
                      this.offsetX = moveX
                    } else if (-moveX > this.bottomViewRightW) {
                      this.offsetX = -this.bottomViewRightW
                      this.sX = event.touches[0].screenX
                      this.leftSlideEnable = false
                    }
                  }

                } else if (moveX > 0 && moveY < 30 && moveY > -30) { //右滑
                  //右侧菜单开始关闭
                  if (this.offsetX < 0) {
                    this.leftSlideEnable = false
                    if (moveX <= this.bottomViewRightW) {
                      this.offsetX = moveX - this.bottomViewRightW
                      return
                    } else {
                      this.offsetX = 0
                    }
                  }


                  if (!this.enableSlide[0]) {
                    return
                  }
                  //当前移动距离小于左侧View的宽度  并且没有处于刚好拉出的状态
                  if (this.leftSlideEnable) {
                    if (this.offsetX >= 0 && this.offsetX < this.bottomViewLeftW && moveX <= this.bottomViewLeftW) {
                      this.offsetX = moveX
                    } else if (moveX > this.bottomViewRightW) {
                      this.offsetX = this.bottomViewLeftW
                      this.sX = event.touches[0].screenX
                      this.rightSlideEnable = false
                    }
                  }
                }
              }
            } else { //纵向滑动
              if (this.offsetX == 0) {
                if (moveY > 0) { //下滑
                  //底部菜单开始关闭
                  if (this.offsetY < 0) {
                    if (moveY - this.mHeight <= 0) {
                      this.offsetY = moveY - this.mHeight
                      return
                    }
                  }

                  if (!this.enableSlide[1]) {
                    return
                  }
                  //当前移动距离小于右侧View的宽度，并且没有处于刚好拉出的状态
                  if (this.offsetY < this.mHeight) {
                    this.offsetY = moveY
                  } else {
                    this.offsetY = this.mHeight
                  }
                } else if (moveY < 0) { //上滑
                  //顶侧菜单开始关闭
                  if (this.offsetY > 0) {
                    if (this.mHeight - Math.abs(moveY) <= 0) {
                      this.offsetY = this.mHeight - moveY
                      return
                    }
                  }


                  if (!this.enableSlide[3]) {
                    return
                  }
                  //当前移动距离小于左侧View的宽度  并且没有处于刚好拉出的状态
                  if (Math.abs(this.offsetY) < this.mHeight) {
                    this.offsetY = moveY
                  } else {
                    this.offsetY = -this.mHeight
                  }
                }
              }
            }
            this.previousX = event.touches[0].screenX
          }

          if (event.type === TouchType.Up) {
            let moveX = event.touches[0].screenX - this.sX;
            let moveY = event.touches[0].screenY - this.sY;
            animateTo({
              duration: this.mDuration, // 动画时长
              tempo: 1, // 播放速率
              curve: Curve.FastOutLinearIn, // 动画曲线
              iterations: 1, // 播放次数
              playMode: PlayMode.Normal, // 动画模式
              onFinish: () => {
                this.rightSlideEnable = true
                this.leftSlideEnable = true
              }
            }, () => {
              if (this.offsetX > 0 && this.leftSlideEnable) {
                if (moveX >= 40) {
                  this.offsetX = this.bottomViewLeftW
                } else if (moveX > 0) {
                  this.offsetX = 0
                }
                if (moveX < -40) {
                  this.offsetX = 0
                } else if (moveX < 0) {
                  this.offsetX = this.bottomViewLeftW
                }
                return
              }

              if (this.offsetX < 0 && this.rightSlideEnable) {
                if (moveX >= 40) {
                  this.offsetX = 0
                } else if (moveX > 0) {
                  this.offsetX = -this.bottomViewRightW
                }

                if (moveX <= -40) {
                  this.offsetX = -this.bottomViewRightW
                } else if (moveX < 0) {
                  this.offsetX = 0
                }
                return
              }

              if (this.offsetY > 0) {
                if (Math.abs(moveY) >= 20) {
                  this.offsetY = this.mHeight
                } else {
                  this.offsetY = 0
                }
              }

              if (this.offsetY < 0) {
                if (Math.abs(moveY) >= 20) {
                  this.offsetY = -this.mHeight
                } else {
                  this.offsetY = 0
                }
              }

            })
            this.parentIsTouch = true
          }
        })
        .position({ x: this.offsetX, y: 0 })
      }
    }
    .width(this.mWidth)
    .height(this.mHeight)
    .position({ x: 0, y: this.offsetY })
  }
}








